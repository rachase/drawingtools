import numpy as np
from numpy import random
import vsketch

def random_arc(vsk, x, y, 
                max_arc_skew_x = 1. ,
                max_arc_skew_y = 1. ,
                max_rings = 15,
                max_radius = 40,
                min_radius = 5,
                max_arc_length_deg = 90,
                min_arc_length_deg = 10):

    num_rings = vsk.random(1,max_rings)
    for ring in np.arange(num_rings):

        max_ring_rad = max_radius   #the max possible size of the ring
        min_ring_rad = min_radius   #the min possible size of the ring
        
        scale = vsk.random(min_ring_rad, max_ring_rad)
        
        scale_skew_w = vsk.random (1.,max_arc_skew_x)
        scale_skew_h = vsk.random (1.,max_arc_skew_y)

        w = scale/scale_skew_w
        h = scale/scale_skew_h

        arc_distance = random.ranf(50)*(np.pi * 2)/16
        new_start = random.ranf(1)[0]*(np.pi * 2)
        full_stop = new_start + (np.pi * 2)*vsk.random(0.2, 1.)
        new_stop = arc_distance[0] + new_start

        for count, dist in enumerate(arc_distance):
            if (count % 2) == 0:
                vsk.arc(x, y, w, h, new_start, new_stop)
            new_start = new_stop
            new_stop = dist + new_start
            if new_stop > full_stop:
                break

class RandomPolygonsSketch(vsketch.SketchClass):

    #set up some inputs
    rows = vsketch.Param(value=5, min_value=1, step=1)
    columns = vsketch.Param(value=8, min_value=1, step=1)
    row_offset = vsketch.Param(40.0)
    column_offset = vsketch.Param(40.0)
    plot_type = vsketch.Param(value=1, min_value=1, max_value=2, step=1)
    max_arc_skew_x = vsketch.Param(1.,min_value=1.0,step=.1)
    max_arc_skew_y = vsketch.Param(1., min_value= 1.0,step=.1)
    max_rings = vsketch.Param(20, min_value= 1,step=1)
    min_radius = vsketch.Param(10., min_value= 2.0,step=1)
    max_radius = vsketch.Param(40., min_value= 2.0,step=1)

    def draw(self, vsk: vsketch.Vsketch) -> None:

        #define the plot size , stroke width, detail, etc
        vsk.size("17in", "11in")
        vsk.penWidth("0.15mm", 1)
        vsk.stroke(1)
        vsk.scale("1mm")
        vsk.detail("1mm")

        #for the total number of arcoids? plot in grid or rando
        for row in np.arange(self.rows):
            for col in np.arange(self.columns):
                if self.plot_type == 1:
                    x = col * self.column_offset
                    y = row * self.row_offset
                else:
                    x = vsk.random(0,vsk.width)/5
                    y = vsk.random(0,vsk.height)/5

                random_arc(vsk,x,y,
                            vsk.random(1,self.max_arc_skew_x),
                            vsk.random(1,self.max_arc_skew_y),
                            vsk.random(1,self.max_rings),
                            vsk.random(self.min_radius,self.max_radius),
                            vsk.random(1,self.min_radius))

    def finalize(self, vsk: vsketch.Vsketch) -> None:
        vsk.vpype("linesimplify linesort")


if __name__ == "__main__":
    RandomPolygonsSketch.display()

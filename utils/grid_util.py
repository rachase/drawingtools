import numpy as np
import matplotlib.pyplot as plt


def grid(num_x,num_y, xlog = False, ylog = False):
    """
    Calculates endpoints of a grid. 
    Returns - 2 x 2 x N ndarray
    1st dim: 0 = start point, 1 = end point
    2nd dim: 0 = x coord, 1 = y coord
    3rd dim: line instance

    :param num_x,num_y: number of x and y lines
    :param xlog, ylog: boolean if log or lin space
    """


    if xlog:
        xs = np.logspace(-1,.041392685,num_x)-.1
    else:
        xs = np.linspace(0,1,num_x)

    if ylog:
        ys = np.logspace(-1,.041392685,num_y)-.1
    else:
        ys = np.linspace(0,1,num_y)

    thisgrid, X_Mesh, Y_mesh = customgrid(xs,ys)
    return thisgrid, X_Mesh, Y_mesh

def customgrid(xs,ys):
    """
    Calculates endpoints of a grid. 
    Returns - 2 x 2 x N ndarray
    1st dim: 0 = start point, 1 = end point
    2nd dim: 0 = x coord, 1 = y coord
    3rd dim: line instance

    :param xs,ys: the values where the grid will go
    """

    # max and min of the window
    ymax = ys.max()
    ymin = ys.min()
    xmax = xs.max()
    xmin = xs.min()

    # 2 x 2 x N
    # 1st dim 0 = start point, 1 = end point
    # 2nd dim 0 = x, 1 = y
    # 3rd dim = line index
    lines = np.ndarray(shape=(2,2, len(xs)+len(ys)))

    # make the lines
    for i in range(len(xs)):
        # start points
        lines[0][0][i] = xs[i] 
        lines[0][1][i] = ymin
        # end points  
        lines[1][0][i] = xs[i] 
        lines[1][1][i] = ymax  

    for i in range(len(ys)):
        # start points
        lines[0][0][i+len(xs)] = xmin 
        lines[0][1][i+len(xs)] = ys[i]
        # end points  
        lines[1][0][i+len(xs)] = xmax 
        lines[1][1][i+len(xs)] = ys[i]  

    X_Mesh,Y_mesh = np.meshgrid(xs,ys)

    return lines, X_Mesh, Y_mesh

def test():
    A, X_Mesh, Y_Mesh = grid(20,30,xlog=False, ylog=False)
    #A = customgrid(np.random.random(size=(10,1)),np.random.random(size=(10,1)))
    for i in range (len(A[0][0][:])):
        xo = A[0][0][i]
        yo = A[0][1][i]

        xf = A[1][0][i]
        yf = A[1][1][i]

        print(f"{xo},{yo},{xf},{yf}")
        plt.plot((xo,yo),(xf,yf))
    plt.scatter(X_Mesh,Y_Mesh, marker='o', color="black")
    plt.show()

if __name__ == '__main__':
    test()
    
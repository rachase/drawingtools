import numpy as np
import vsketch
from shapely import affinity

class Marker(vsketch.SketchClass):

    #set up some inputs
    threshold = vsketch.Param(False)
    rows = vsketch.Param(value=4, min_value=1, step=1)
    columns = vsketch.Param(value=4, min_value=1, step=1)
    # row_offset = vsketch.Param(60.0)
    # column_offset = vsketch.Param(60.0)

    marker_scale = vsketch.Param(10.0)
    marker_spacing = vsketch.Param(10.0)
    marker_obj_cnt_x = vsketch.Param(value=4, min_value=1, step=1)
    marker_obj_cnt_y = vsketch.Param(value=4, min_value=1, step=1)


    noise_scale = vsketch.Param(1.0,decimals=2)
    marker_diff = vsketch.Param(0.0,decimals=4)

    def draw(self, vsk: vsketch.Vsketch) -> None:

        #setup some drawing parameters
        vsk.size("17in", "11in",center=True)
        vsk.penWidth("0.2mm")
        vsk.detail(".001mm")   
        vsk.stroke(1)

        #iterate over grid
        count = 0
        for X in np.arange(self.rows):
            for Y in np.arange(self.columns):

                vsk.stroke(count+1)
                diff = self.marker_diff*count
                threshold_val = vsk.random(.3,.6)
                if (self.threshold == False):
                    threshold_val = 0
                for row in np.arange(self.marker_obj_cnt_x):
                    for col in np.arange(self.marker_obj_cnt_y):
                        
                        x_pos = row*self.marker_spacing + X*(self.marker_obj_cnt_x*(self.marker_spacing+2))
                        y_pos = col*self.marker_spacing + Y*(self.marker_obj_cnt_y*(self.marker_spacing+2))
                        noise_x_seed = row*(self.noise_scale)+9123.324
                        noise_y_seed = col*(self.noise_scale)+234.123
                        noise_val = vsk.noise(noise_x_seed,noise_y_seed,diff)
                        if (noise_val > threshold_val):
                            radius = (noise_val**2)*(self.marker_scale)
                            vsk.circle(x_pos,y_pos,radius)
                            #vsk.square(x_pos,y_pos,radius)
                            
                        
                
                count = count + 1


    def finalize(self, vsk: vsketch.Vsketch) -> None:
        vsk.vpype("linesimplify linesort perspective")        

if __name__ == "__main__":
    Marker.display()



import numpy as np
import vsketch

class Plot1117(vsketch.SketchClass):
    row_count = vsketch.Param(7, 1)
    column_count = vsketch.Param(7, 1)
    row_offset = vsketch.Param(100.0)
    column_offset = vsketch.Param(100.0)

    def draw(self, vsk: vsketch.Vsketch) -> None:
        vsk.size("17in", "11in")

    def finalize(self, vsk: vsketch.Vsketch) -> None:
        vsk.vpype("linesimplify linesort")        

if __name__ == "__main__":
    Plot1117.display()

import numpy as np
import vsketch
import matplotlib.pyplot as plt

import os,sys
sys.path.append(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))
from utils import grid_util
#  sys.path.append(os.path.abspath('../utils'))

class Topomap(vsketch.SketchClass):
    
    dots = vsketch.Param(False)
    spacing = vsketch.Param(20, step=1)
    noise_scale = vsketch.Param(1.0, step=.001, decimals= 3)

    num_rows = vsketch.Param(200, min_value= 1,step=1)
    num_columns = vsketch.Param(200, min_value= 1,step=1)

    levels = vsketch.Param( 12, min_value= 1, max_value=128, step=1)
    draw_lines = vsketch.Param(False)
    draw_marks = vsketch.Param(False)
    y_lines = vsketch.Param(10, step=1)
    x_lines = vsketch.Param(10, step=1)
    xlog = vsketch.Param(False)
    ylog = vsketch.Param(False)

    skpdbl_val = vsketch.Param(2, step=1)
    skpdbl_prob = vsketch.Param(0.5, step=.10)
    dotted_prob = vsketch.Param(0.5, step=.10)


    def draw(self, vsk: vsketch.Vsketch) -> None:

        scale = 40
        #setup some drawing parameters
        vsk.size("17in", "11in")
        vsk.penWidth("0.2mm")
        vsk.detail(".001mm")   
        vsk.stroke(1)


        xs = np.linspace(0,12,self.num_columns)
        ys = np.linspace(0,8,self.num_rows)

        X_mesh, Y_mesh = np.meshgrid(xs,ys)

        vsk.scale(scale,scale)
       
        noise = vsk.noise((xs/self.noise_scale)+123.45,(ys/self.noise_scale)+678.91)
        Z_mesh = np.swapaxes(noise,0,1)
       
        thing = plt.contour(X_mesh*self.spacing,Y_mesh*self.spacing,Z_mesh-0.5,self.levels,colors='k')

        count = 0 
        for level in range(len(thing.allsegs)):
            #vsk.stroke(level+1)
            for segment in range(len(thing.allsegs[level])):
                dot_flag = vsk.randomGaussian() < self.dotted_prob
                skip = np.random.randint(2,5)
                if (vsk.randomGaussian() < self.skpdbl_prob):
                    skip = skip*self.skpdbl_val
                
                if (self.dots):
                    for point in range(len(thing.allsegs[level][segment])):
                        xo = thing.allsegs[level][segment][point][0]
                        yo = thing.allsegs[level][segment][point][1]
                        if ((count % self.skpdbl_val) == 0):
                            vsk.point(xo,yo)
                        count = count + 1
                else:    
                    #draw lines
                    for point in range(len(thing.allsegs[level][segment])-1):
                        xo = thing.allsegs[level][segment][point][0]
                        yo = thing.allsegs[level][segment][point][1]
                        xf = thing.allsegs[level][segment][point+1][0]
                        yf = thing.allsegs[level][segment][point+1][1]
                        if dot_flag:
                            #vsk.stroke(50+level)
                            if ((count % skip) == 0):
                                vsk.line(xo,yo,xf,yf)
                        else:
                            #vsk.stroke(150+level)
                            vsk.line(xo,yo,xf,yf)
                        count = count + 1
                    
                    


        pltgrid,X_mesh,Y_mesh = grid_util.grid(self.x_lines,self.y_lines,xlog=self.xlog,ylog=self.ylog)
        if(self.draw_lines):
            vsk.stroke(200)
            for i in range (len(pltgrid[0][0][:])):
                xo = pltgrid[0][0][i]*scale/2
                yo = pltgrid[0][1][i]*scale/2
                xf = pltgrid[1][0][i]*scale/2
                yf = pltgrid[1][1][i]*scale/2

                #print(f"i = {i} - {xo},{yo},{xf},{yf}")
                vsk.line(xo,yo,xf,yf)

        if(self.draw_marks):
            X = X_mesh.flatten()
            Y = Y_mesh.flatten()
            for i in range((X_mesh.size)):
                vsk.stroke(201)
                vsk.circle(X[i]*scale/2,Y[i]*scale/2,.2)
                vsk.stroke(202)
                vsk.circle(X[i]*scale/2,Y[i]*scale/2,.4)

       

    def finalize(self, vsk: vsketch.Vsketch) -> None:
        vsk.vpype("linesimplify linesort perspective")        

if __name__ == "__main__":
    Topomap.display()

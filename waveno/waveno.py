# https://scipython.com/blog/packing-circles-in-a-circle/
# https://scipython.com/blog/packing-circles-inside-a-shape/

import numpy as np
import vsketch
import matplotlib.pyplot as plt
import time

class Waveno(vsketch.SketchClass):

    wave_num = vsketch.Param( 0.5, min_value= 0, max_value=10, step=.1)
    threshold = vsketch.Param(0.00, min_value= -1.00,max_value=1.00, step=0.10)
    period = vsketch.Param( 4, min_value= 1, max_value=20, step=1)
    phase = vsketch.Param( 0, min_value= 0, max_value=30, step=1, decimals=0)
    step = vsketch.Param( 1, min_value= 1, max_value=20, step=1)

    spacial_scale = vsketch.Param( 20, min_value= 1, max_value=100, step=1)
    circle_scale = vsketch.Param( 20, min_value= 1, max_value=100, step=1)

   
    def draw(self, vsk: vsketch.Vsketch) -> None:

        #setup some drawing parameters
        vsk.size(height="11in", width="11in",center=True)
        vsk.penWidth("0.2mm")
        vsk.detail(".001mm")
        vsk.stroke(4)

        # setup the extents and step
        x_min = y_min = -self.period*np.pi
        x_max = y_max =  self.period*np.pi
        dx = self.step*np.pi/10
        
        # wave number
        k = 2 * np.pi / self.wave_num

        xs = np.linspace(x_min,x_max,int(x_max/dx))
        ys = xs

        (X_mesh, Y_mesh) = np.meshgrid(xs, ys)

        Z_vals = np.cos(k*np.sqrt(X_mesh**2 + Y_mesh**2)) 
       
        Z_vals_threshed =  Z_vals < self.threshold
        result = np.where(Z_vals_threshed == True)

        x_vals = result[0]
        y_vals = result[1]
        
        for i in np.arange(len(x_vals)-self.phase):
            diam = np.abs(Z_vals[x_vals[i],y_vals[i]]+1)*self.circle_scale
            x_coord = x_vals[i+self.phase]*self.spacial_scale
            y_coord = y_vals[i+self.phase]*self.spacial_scale
            vsk.circle(x = x_coord, y = y_coord, diameter=diam)

    def finalize(self, vsk: vsketch.Vsketch) -> None:
        vsk.vpype("linesimplify linesort")        

if __name__ == "__main__":
    Waveno.display()

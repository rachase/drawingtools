import numpy as np
import vsketch

class Field(vsketch.SketchClass):
    
    spacing = vsketch.Param(0.225, step=.100)
    noise_scale = vsketch.Param(10.0, step=.10)

    num_rows = vsketch.Param(50, min_value= 1,step=1)
    num_columns = vsketch.Param(35, min_value= 1,step=1)
    
    def get_line(self,x,y,len,rot):

        xf = len*np.cos(rot)
        yf = len*np.sin(rot)

        # center is midpoint of line
        sx = xf/2
        sy = yf/2

        #sx = yx = 0
        return x-sx,y-sy,xf+x-sx,yf+y-sy

    def draw(self, vsk: vsketch.Vsketch) -> None:

        #setup some drawing parameters
        vsk.size("17in", "11in")
        vsk.penWidth("0.2mm")
        vsk.detail(".001mm")   
        vsk.stroke(1)
        scale =100

        asi = .4

        #print(f"xo:{startx}  yo:{starty}  xf:{endx}  yf:{endy}")
        ex = self.spacing*self.num_rows
        ey = self.spacing*self.num_columns
        x_space = np.linspace(-ex,ex,self.num_rows)
        y_space = np.linspace(-ey,ey,self.num_columns)

        vsk.scale(40,40)
        #vsk.rotate(np.pi/2)
        #vsk.shape(shape)

        ns = self.noise_scale
        for x in x_space:
            for y in y_space:
                rotation = vsk.noise(x/ns+123.456,y/ns+789.012)*np.pi*2
                xo,yo,xf,yf = self.get_line(x,y,asi,rotation)
                #print(xo,yo,xf,yf)
                vsk.stroke(1)
                vsk.line(xo,yo,xf,yf)
                vsk.stroke(2)
                #vsk.circle(x,y,.25)
                vsk.arc(x,y,asi,asi,start=(-rotation-.5),stop=(-rotation+.5))
                
    def finalize(self, vsk: vsketch.Vsketch) -> None:
        vsk.vpype("linesimplify linesort perspective")        

if __name__ == "__main__":
    Field.display()

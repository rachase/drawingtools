import numpy as np
import vsketch

class MicroAsteroid(vsketch.SketchClass):

    #Start Radius
    #start_radius = vsketch.Param(0.0000, min_value= 0.000,step=.001)
    start_radius = 0
    
    radius_step = vsketch.Param(0.2, min_value= 0.000,step=.001)
    ring_count = vsketch.Param(75,1)
    ring_scale = vsketch.Param(1.15, min_value= 0.000,step=.01,decimals = 2)

    # Low Freq Noise
    #noise_global_lf = vsketch.Param(50.00, min_value= 0.000,max_value = 200.0, step=.01)
    #noise_y_lf = vsketch.Param(0.0500, min_value= 0.0000,step=.0001,decimals=4)
    #noise_x_lf = vsketch.Param(0.0500, min_value= 0.0000,step=.0001,decimals=4)

    #fixed global lf noise values. x,y noises are random between .05 .10
    noise_global_lf = 50
    noise_y_lf = .05
    noise_x_lf = .05

    # High Freq Noise
    # noise_global_hf = vsketch.Param(.50, min_value= 0.000,max_value = 200.0, step=.01)
    # noise_y_hf = vsketch.Param(1.000, min_value= 0.000,step=.001)
    # noise_x_hf = vsketch.Param(1.000, min_value= 0.000,step=.001)

    #fixed hf noise
    noise_global_hf = .5
    noise_y_hf = .5
    noise_x_hf = .5

    #line removal
    line_removal = vsketch.Param(1, min_value= 1,step = 1,decimals=0)
    #line_removal = 1

    fov_deg = vsketch.Param(90, min_value= 0,step = 5,decimals=0)
    tilt_deg = vsketch.Param(0, min_value= -90,step = 5,decimals=0)
    scale_global = vsketch.Param(5., min_value= 1.0,step=.01)

    #lf global = 10, noise x y between .1 and .3

    def draw(self, vsk: vsketch.Vsketch) -> None:

        #setup some drawing parameters
        vsk.size("6in", "4in",center=False)
        vsk.penWidth("0.2mm")
        vsk.detail(".001mm")
        vsk.stroke(4)

        #random select x,y noise scale between vals.  
        self.noise_y_lf = vsk.random(.05,.10)
        self.noise_x_lf = self.noise_y_lf

        #self.line_removal = np.random.choice([1, 2, 4], size=1)
        
        # get an array of angles from 0 -360  (Nx1)
        theta = np.linspace(start=0,stop=(np.pi*2),num=628)
        #generate the different radii (Mx1)
        radii = np.linspace(start=self.start_radius,stop=((self.ring_count*self.radius_step)+self.start_radius),num=self.ring_count)
                
        #make the rings
        for count in np.arange(self.ring_count):
            #set the layer
            vsk.stroke((self.ring_count+1)-count)

            radius = radii[count]*self.ring_scale

            xn = (radius) * np.cos(theta)
            yn = (radius) * np.sin(theta)
            
            radius_based_scale = (radii[count]/np.max(radii))**(1/1)
            #radius_based_scale = 1
            radius_noise_lf = vsk.noise((12345.90 + xn)*self.noise_x_lf, (7654.765 + yn)*self.noise_y_lf , grid_mode=False)*(self.noise_global_lf*radius_based_scale)
            radius_noise_hf = vsk.noise((12345.90 + xn)*self.noise_x_hf, (7654.765 + yn)*self.noise_y_hf , grid_mode=False)*(self.noise_global_hf*radius_based_scale)
            xp = (radius+radius_noise_lf+radius_noise_hf) * np.cos(theta)
            yp = (radius+radius_noise_lf+radius_noise_hf) * np.sin(theta)

            if ((count % self.line_removal) == 0):
                vsk.polygon(x=xp*self.scale_global, y=yp*self.scale_global,close=True)

        
        vsk.vpype(f"pspread .025cm ptranslate 8cm -5cm 0 color blue")
        vsk.vpype(f"perspective -f {self.fov_deg} --tilt {self.tilt_deg}" )
        #vsk.vpype("crop 5mm 5mm '%prop.vp_page_size[0]-1*cm%' '%prop.vp_page_size[1]-1*cm%'")
        vsk.vpype("occult -i")


    def finalize(self, vsk: vsketch.Vsketch) -> None:
        vsk.vpype("linesimplify linesort")        

if __name__ == "__main__":
    MicroAsteroid.display()

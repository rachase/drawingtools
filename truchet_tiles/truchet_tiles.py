import numpy as np
import vsketch

class TruchetTiles(vsketch.SketchClass):
    
    slope = vsketch.Param(1.0, step=.10)
    num_lines = vsketch.Param(11, min_value= 1,step=1)

    num_rows = vsketch.Param(11, min_value= 1,step=1)
    num_columns = vsketch.Param(11, min_value= 1,step=1)
    extents = vsketch.Param(5.0, min_value= 1.0,step=.10)

    translate_last = vsketch.Param(False)
    
    # something going on where need to do num_lines + 2
    def make_a_tile(self, num_lines, slope):
        #unit box size is 1x1 

        num_lines = num_lines + 2
        #num_lines = 41 # the number of lines in the truchet need to be odd
        #slope = 1/1    # the slope of the lines
        
        offsets = np.linspace(-slope,1,num_lines)
        
        startx = np.zeros(offsets.shape)  #starting values of x will always be 0
        starty = offsets                  # y = mx + b -> y = b
        endy = startx+1.                  # y ends at top of box, so 1
        endx = (1.-offsets)/slope         # solve for final x
          
        for i in range(num_lines):

            # if the starting point is below the y axis, new x is -b/m
            # new y would by y axis, so 0
            # this only works for straight lines
            if (starty[i] < 0):
                startx[i] = -starty[i]/slope
                starty[i] = 0
            
            # check if the line ends above x > 1
            if (endx[i] > 1):
                #how far is x from x=1?
                distance = endx[i]- 1
                #get angle
                theta = np.arctan(slope)
                # the distance to y = 1
                dist_to_box = distance * np.tan(theta)
                # we know the distance to the edge of occlusion box, 
                # we want that distance to line y = 0, not y = 1
                endy[i] = 1 - dist_to_box
                endx[i] = 1

        # check to make sure its a line, and not a point, remove poitns
        # make a some sets of start and end points
        starts = np.array((startx,starty))
        ends = np.array((endx, endy))
        #find the distance
        dist = np.sqrt(np.sum((ends - starts) ** 2, axis=0))
        #check if distance is close to zero, remove if so
        idxs = np.isclose(dist,0)
        startx = np.delete(startx, idxs)
        starty = np.delete(starty, idxs)
        endx = np.delete(endx, idxs)
        endy = np.delete(endy, idxs)

        # shift origin to center
        startx = startx - .5
        starty = starty - .5
        endx = endx - .5
        endy = endy - .5

        # return
        return startx,starty,endx,endy

    def draw(self, vsk: vsketch.Vsketch) -> None:

        #setup some drawing parameters
        vsk.size("17in", "11in")
        vsk.penWidth("0.2mm")
        vsk.detail(".001mm")   
        vsk.stroke(1)
        scale =100

        #construct the tile    
        startx,starty,endx,endy = self.make_a_tile(self.num_lines,self.slope)
        #print(f"xo:{startx}  yo:{starty}  xf:{endx}  yf:{endy}")

        shape = vsk.createShape()
        for i in range(len(startx)):
            #print(f"{i}\txo:{startx[i]:.3f}  yo:{starty[i]:.3f}  xf:{endx[i]:.3f}  yf:{endy[i]:.3f}")
            shape.line(startx[i],starty[i],endx[i],endy[i])

        #self.extents = 5
        x_space = np.linspace(-self.extents,self.extents,self.num_rows)
        y_space = np.linspace(-self.extents,self.extents,self.num_columns)

        vsk.scale(80,80)
        #vsk.rotate(np.pi/2)
        #vsk.shape(shape)

        for x in x_space:
            for y in y_space:
                vsk.pushMatrix()
        
                # do translation first
                if (not self.translate_last):
                    vsk.translate(x,y)

                # a random rotation
                flag = vsk.noise(float(x)+113.123,float(y)+14142.23) < .5
                if flag:
                    vsk.stroke(2)
                    vsk.rotate(np.pi/2)

                # do translation last
                if (self.translate_last):
                    vsk.translate(x,y)

                vsk.shape(shape)
                vsk.popMatrix()
                vsk.stroke(1)

    def finalize(self, vsk: vsketch.Vsketch) -> None:
        vsk.vpype("linesimplify linesort perspective")        

if __name__ == "__main__":
    TruchetTiles.display()

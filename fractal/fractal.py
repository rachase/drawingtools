# https://scipython.com/blog/packing-circles-in-a-circle/
# https://scipython.com/blog/packing-circles-inside-a-shape/

import numpy as np
import vsketch
import matplotlib.pyplot as plt
import time

class Fractal(vsketch.SketchClass):

    decnum = 6
    grid_size = vsketch.Param( 100, min_value= 10, max_value=1000, step=1)
    xc = vsketch.Param( -1.000300, min_value= -1.000300, max_value=100.000000, step=.001, decimals=6)
    yc = vsketch.Param( 0.304000, min_value= -1.000000, max_value=100.000000, step=.001, decimals=6)
    w = vsketch.Param( 0.1, min_value= 0.0000, max_value=100.0000, step=.001, decimals=6)

    cutoff = vsketch.Param( 4.00, min_value= 0.00, max_value=100.00, step=.100, decimals=6)
    # wave_num = vsketch.Param( 2., min_value= -10, max_value=10, step=.01)
    
    #scale = vsketch.Param(1, min_value= 1,max_value=100000, step=1)
    levels = vsketch.Param( 12, min_value= 1, max_value=50, step=1)


    def mandelbrot(self,c, threshold,cutoff):
        """Calculates whether the number c = x + i*y belongs to the
        Mandelbrot set. In order to belong, the sequence z[i + 1] = z[i]**2 + c
        must not diverge after 'threshold' number of steps. The sequence diverges
        if the absolute value of z[i+1] is greater than 4.

        :param float c: the initial complex number
        :param int threshold: the number of iterations to considered it converged
        """

        # initial conditions
        z = np.zeros_like(c, dtype=complex)
        n_out = np.zeros_like(z, dtype=int)
        valid = np.ones_like(z, dtype=bool)

        for i in range(threshold):
            z[valid] = z[valid] ** 2 + c[valid]
            valid = (abs(z) < cutoff)
            n_out[valid] += 1

        return n_out

    def generate_complex_grid(self,xc,yc,w,n_grid):
        real = np.linspace(xc-w/2,xc+w/2,n_grid)
        imag = np.linspace(yc-w/2,yc+w/2,n_grid)
        #get two grids with 
        (Xr, Xi) = np.meshgrid(real, imag)
        c = Xr + 1j * Xi
        return real, imag, c


    def this_main(self):

        threshold = 100
        xc = -1 + 0.0003
        yc = 0.304
        w = 0.01

        x, y, c = self.generate_complex_grid(self.xc, self.yc, self.w, self.grid_size)
        ms = self.mandelbrot(c, threshold,self.cutoff)

        return ms,x,y

      
    def draw(self, vsk: vsketch.Vsketch) -> None:

        #setup some drawing parameters
        vsk.size(height="6in", width="6in",center=True)
        vsk.penWidth("0.2mm")
        vsk.detail(".0001mm")
        vsk.stroke(4)

        Z_Mesh,X_Mesh,Y_Mesh = self.this_main()

        pixels = np.linspace(0,1,self.grid_size)
    

        thing = plt.contour(pixels,pixels,Z_Mesh,self.levels)

        vsk.scale(500,500)

        for level in range(len(thing.allsegs)):
            vsk.stroke(level+1)
            for segment in range(len(thing.allsegs[level])):
                for point in range(len(thing.allsegs[level][segment])-1):
                    xo = thing.allsegs[level][segment][point][0]
                    yo = thing.allsegs[level][segment][point][1]
                    xf = thing.allsegs[level][segment][point+1][0]
                    yf = thing.allsegs[level][segment][point+1][1]
                    vsk.line(xo,yo,xf,yf)
                    #vsk.circle(xo,yo,level*1.2)

    def finalize(self, vsk: vsketch.Vsketch) -> None:
        vsk.vpype("linesimplify linesort")        

if __name__ == "__main__":
    Fractal.display()

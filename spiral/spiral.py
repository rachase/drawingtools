import numpy as np
import vsketch


class Spiral(vsketch.SketchClass):
    radius_step = vsketch.Param(.5, min_value= 0.0,step=.01)
    spiral_scale = vsketch.Param(0.100, min_value= 0.0,step=.001)
    num_steps = vsketch.Param(5000, min_value= 1,step=100)
    arc_step_length = vsketch.Param(6.00, min_value= 0.0,step=.1)

    noise_global = vsketch.Param(0, min_value= 0.0,max_value = 200.0, step=.01)
    noise_rad = vsketch.Param(1.00, min_value= 0.0,step=.001)
    noise_theta = vsketch.Param(1.00, min_value= 0.0,step=.001)
    scale = vsketch.Param(100.0, min_value= 1.0,step=2)
    noise_octaves = vsketch.Param(1, min_value= 1.0,step=1)
    noise_falloff = vsketch.Param(0.0, min_value= 0.0,step=.1)


    def next_arc(self,target_arclength, arc_param, starting_theta):
        
        p1 = (starting_theta*np.sqrt(1+(starting_theta**2))) + np.arcsinh(starting_theta)

        length = 0
        theta_new = starting_theta
        step_size = .001

        while ((np.abs(length) < target_arclength)):
            # calc the new param
            p2 = (theta_new*np.sqrt(1+(theta_new**2)))+np.arcsinh(theta_new)
            # calc the arc length
            length = (arc_param*0.5)*(p2-p1)
            # see if its within tolerance, if not inc step size
            if (np.abs(length) < target_arclength):
                theta_new = theta_new + step_size

        return (theta_new)

    def draw(self, vsk: vsketch.Vsketch) -> None:

        #setup some drawing parameters
        vsk.size("6in", "4in")
        vsk.penWidth("0.2mm")
        vsk.detail(".001mm")
        vsk.stroke(4)
        vsk.noiseDetail(self.noise_octaves,self.noise_falloff)
        

        #get the thetas for a specified arc length
        temp_theta = 0
        equispaced_theta = np.zeros(self.num_steps)
        for i in range(0, self.num_steps):
            equispaced_theta[i] = self.next_arc(self.arc_step_length,self.radius_step,temp_theta)
            temp_theta=equispaced_theta[i]
    
        #generate the different radii (Mx1)
        radii = equispaced_theta*(self.radius_step)

        #make the noise data for the ring (MxN)
        radnoise = radii*self.noise_rad
        thetanoise = equispaced_theta*self.noise_theta
        radius_noise_data = (vsk.noise(radnoise,thetanoise,grid_mode=False)-.5)*self.noise_global
        
        print(f"shape is {radius_noise_data.shape}")
        xp = (radii+radius_noise_data) * np.cos(equispaced_theta)
        yp = (radii+radius_noise_data) * np.sin(equispaced_theta)
        vsk.polygon(x=xp,y=yp,close=False)

    def finalize(self, vsk: vsketch.Vsketch) -> None:
        vsk.vpype("linesimplify linesort")        

if __name__ == "__main__":
    Spiral.display()
